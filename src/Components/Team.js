import React from 'react';
import '../style/Team.css'
import TeamCard from './TeamCard'

export default function Team(props){
    const renderTeams = () => {
        let teams = []
        for(let i = 0; i < props.numberOfTeams; i++) {
            teams.push(<TeamCard />)
        }
        return teams
    }
    
    return (
        <>
            <div class="teams-div">
                {renderTeams()}
            </div>
        </>
    )
}