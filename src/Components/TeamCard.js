import React, { useState } from 'react';
import '../style/TeamCard.css'

export default function TeamCard() {
    const[teamName, setTeamName] = useState("");
    const[teamOwner, setTeamOwner] = useState("");

    return(
        <>
            <div class="team-card-info-layout">
                <p name="name">Team Name: <input labelFor="name" type="text" onChange={event => setTeamName(event.target.value)}/></p>
                <p name="owner">Team Owner: <input labelFor="owner" type="text" onChange={event => setTeamOwner(event.target.value)}/></p>
            </div>
            
        </>
    )
}