import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import Team from './Components/Team';
import PlayersAvail from './Components/PlayersAvail';


function App() {
  const [teams, setTeamsCount] = useState(0);

  return (
    <div className="App">
      <h1>hello world</h1>
      <button onClick={() => {setTeamsCount(teams + 1)}}>Add a team!</button>
      <h2>Number of teams: {teams}</h2>
      <Team numberOfTeams={teams}/>
      <button onClick={() => {setTeamsCount(teams - 1)}}>Remove a team!</button>
      <PlayersAvail />
    </div>
  );
}

export default App;
